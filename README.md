![screenshot](https://steamuserimages-a.akamaihd.net/ugc/940572502835023694/8E8B821EAC54814FC8E3520D6478DA7EE911A2DE/)

# Howto
This is suited for [barebones](https://gitlab.com/alaah/dead-cells-barebones).

# License
See LICENSE for information
Of course this is based on absolutely proprietary files. I consider my "changes" to be GPL. No one will care anyway.