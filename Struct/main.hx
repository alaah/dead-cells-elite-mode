function buildMainRooms()
{
    Struct.createSpecificRoom("aEMEntrance")
        .setName("start")
        .chain(Struct.createSpecificRoom("aEMDoor1"));
    Struct.createRunicZDoor(Struct.createSpecificExit("PrisonStart", "aEMExit"), 4, Struct.allRooms);
    // Struct.createRunicZDoor(Struct.createExit("PrisonStart"), 4, Struct.allRooms);
}

function buildSecondaryRooms()
{

}

function addTeleports()
{

}

function buildTimedDoors()
{

}

function finalize()
{

}
function buildMobRoster(_mobList)
{
    addMobRosterFrom("PrisonStart", _mobList);
}

function setLevelProps(_levelProps)
{
    setLevelPropsFrom("PrisonStart", _levelProps);
    _levelProps.musicIntro = "music/ambiant2.ogg";
    _levelProps.musicLoop = "music/ambiant2.ogg";
}

function setLevelInfo(_levelInfo)
{
    setLevelInfoFrom("PrisonStart", _levelInfo);
    _levelInfo.mobDensity = 0.0;
    _levelInfo.tripleUps = 0;
    _levelInfo.eliteRoomChance = 0;
}
